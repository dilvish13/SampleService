package ru.shadewallcorp.itdepart.order.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * Информация о заказе (API сервиса).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@AllArgsConstructor
@Builder
@Value
public class OrderSimple {

    private final String id;
    @JsonFormat(pattern = "yyyy.MM.dd'T'HH:mm:ssZ")
    private final ZonedDateTime creationDate;
    private final String orderState;
    private final String orderType;
    private final String region;
    private final String city;
    private final String salePointId;
    private final String salePointAddress;
    private final Integer shipGroupType;
    private final String clientName;
    private final BigDecimal totalPrice;
    private final String payType;
    private final String payState;
    private final String shpi;
    private final String comment;
    /** Набор атрибутов. */
    private final String attrFlag;

}
