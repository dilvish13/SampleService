package ru.shadewallcorp.itdepart.order.api;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.transport.Method;
import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;
import com.lightbend.lagom.javadsl.api.transport.TransportException;
import org.taymyr.lagom.javadsl.openapi.OpenAPIService;

import java.util.List;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.restCall;
import static java.lang.String.format;

/**
 * Сервис поиска заказов.
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
public interface OrderService extends OpenAPIService {

    String SERVICE_NAME = "sampleService";

    /**
     * Получить заказ (полный) по ID.
     * @param id ID
     * @return {@link ServiceCall}
     */
    ServiceCall<NotUsed, List<OrderResponse>> getOrder(Long id);

    /**
     * Получить список всех заказов по фильтру.
     * @return {@link ServiceCall}
     */
    ServiceCall<FilterOrder, List<OrderSimpleResponse>> findOrders();

    /**
     * Записать заказ.
     * @return {@link ServiceCall}
     */
    ServiceCall<Order, String> insertOrder();

    @Override
    default Descriptor descriptor() {
        return withOpenAPI(named(SERVICE_NAME)
                .withCalls(
                        restCall(Method.GET, "/request1/:id", this::getOrder),
                        restCall(Method.POST, "/request2", this::findOrders),
                        restCall(Method.PUT, "/insert", this::insertOrder))
                .withAutoAcl(true));
    }

    /**
     * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
     */
    class ParamRequiredException extends TransportException {
        private static final long serialVersionUID = 1L;

        ParamRequiredException(String param) {
            super(TransportErrorCode.BadRequest, "Param '" + param + "' is required");
        }
    }

    /**
     * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
     */
    class AccessForbiddenException extends TransportException {
        private static final long serialVersionUID = 1L;

        AccessForbiddenException() {
            super(TransportErrorCode.Forbidden, "Access forbidden");
        }
    }

    /**
     * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
     */
    class OrderServiceSystemException extends TransportException {
        private static final long serialVersionUID = 1L;

        public OrderServiceSystemException(String errMsg) {
            super(TransportErrorCode.InternalServerError, format("System exception: %s", errMsg));
        }
    }

    /**
     * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
     */
    class TimeLimitExceededException extends TransportException {
        private static final long serialVersionUID = 1L;

        private static final int CODE = 509;

        TimeLimitExceededException() {
            super(TransportErrorCode.fromHttp(CODE), "Time limit exceeded");
        }
    }

}
