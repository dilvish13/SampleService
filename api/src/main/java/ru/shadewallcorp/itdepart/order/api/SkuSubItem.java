package ru.shadewallcorp.itdepart.order.api;

import lombok.Builder;
import lombok.Value;

import javax.money.MonetaryAmount;

/**
 * Характеристика продукта (API сервиса заказов).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Value
@Builder
public class SkuSubItem {

    private final String articleNumber;
    private final CatalogueId catalogueId;
    private final String type;
    private final String caption;
    private final Integer amount;
    private final MonetaryAmount price;
    private final MonetaryAmount salePrice;

}
