package ru.shadewallcorp.itdepart.order.api;

import lombok.Builder;
import lombok.Value;

/**
 * Характеристика catalogueId продукта (API сервиса заказов).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Value
@Builder
public class CatalogueId {

    private final String productId;
    private final String skuId;
    private final String serialId;

}
