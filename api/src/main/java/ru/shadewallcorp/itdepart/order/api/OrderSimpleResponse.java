package ru.shadewallcorp.itdepart.order.api;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

/**
 * Информация о заказе и служебная информация (API сервиса).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@AllArgsConstructor
@Builder
@Value
public class OrderSimpleResponse {

    @JsonUnwrapped
    private OrderSimple orderSimple;
    private final Long totalPages;

}
