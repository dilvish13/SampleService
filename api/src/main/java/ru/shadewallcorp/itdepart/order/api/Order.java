package ru.shadewallcorp.itdepart.order.api;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.pcollections.PSequence;

/**
 * Заказ (полный) (API сервиса).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@AllArgsConstructor
@Builder
@Value
public final class Order {

    @JsonUnwrapped
    private final OrderSimple orderSimple;
    private final String addressDelivery;
    private final String clientEmail;
    private final String clientPhone;
    private final PSequence<OrderCompositionItem> orderComposition;
    private final String promo;
    private final String parentNetwork;
    private final String network;
    private final String partner;
    private final String cancelReason;

}
