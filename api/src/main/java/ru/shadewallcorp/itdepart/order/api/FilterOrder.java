package ru.shadewallcorp.itdepart.order.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Value;

import java.time.ZonedDateTime;

/**
 *  Параметры для поиска заказов.
 * <br>Сходный набор полей у заказа.
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Builder
@Value
public class FilterOrder {

    @JsonIgnore
    public static final int DEFALUT_ES_PAGE_SIZE = 10;
    @JsonIgnore
    private static final int DEFAULT_ES_FROM = 0;

    private Long id;
    private ZonedDateTime dateFrom;
    private ZonedDateTime dateTo;
    private String orderState;
    private String orderType;
    private String region;
    private String city;
    private String salePointId;
    private Integer shipGroupType;
    private String clientName;
    private String clientEmail;
    private String clientPhone;
    private String payType;
    /** Набор допустимых состояний. */
    private String payState;
    private String comment;
    private String shpi;
    /** Фильтр на поля состава заказа. */
    private String orderComposition;
    private String parentNetwork;
    private String network;
    private String partner;
    private String cancelReason;
    /** Фильтр по заполненности: true - с указанным attrFlag, false - без attrFlag, null - все. */
    private Boolean checkAttrFlag;
    private String promo;
    /** Фильтр по заполненности:  true - с указанным promo, false - без promo, null - все. */
    private Boolean checkPromo;
    private Integer page;
    private Integer pageSize;

    public Integer getFrom() {
        return page != null ? page * (pageSize != null ? pageSize : DEFALUT_ES_PAGE_SIZE) : DEFAULT_ES_FROM;
    }

    /**
     * Искать только с пустыми attrFlag.
     * @return признак
     */
    public boolean checkAttrFlagEmpty() {
        return checkAttrFlag != null && !checkAttrFlag;
    }

    /**
     * Искать только с заполненными attrFlag.
     * @return признак
     */
    public boolean checkAttrFlagNonEmpty() {
        return checkAttrFlag != null && checkAttrFlag;
    }

    /**
     * Искать только с пустыми promo.
     * @return признак
     */
    public boolean checkPromoEmpty() {
        return checkPromo != null && !checkPromo;
    }

    /**
     * Искать только с заполненными promo.
     * @return признак
     */
    public boolean checkPromoNonEmpty() {
        return checkPromo != null && checkPromo;
    }

}
