package ru.shadewallcorp.itdepart.order;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.client.integration.LagomClientFactory;
import lombok.Getter;
import org.javamoney.moneta.Money;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.taymyr.lagom.elasticsearch.ServiceCall;
import org.taymyr.lagom.elasticsearch.document.ElasticDocument;
import org.taymyr.lagom.elasticsearch.document.dsl.update.DocUpdateRequest;
import org.taymyr.lagom.elasticsearch.indices.ElasticIndices;
import org.taymyr.lagom.elasticsearch.indices.dsl.CreateIndex;
import org.taymyr.lagom.elasticsearch.indices.dsl.DataType;
import org.taymyr.lagom.elasticsearch.indices.dsl.Mapping;
import org.taymyr.lagom.elasticsearch.indices.dsl.MappingProperty;
import org.taymyr.lagom.elasticsearch.search.ElasticSearch;
import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;
import ru.shadewallcorp.itdepart.order.elastic.CatalogueId;
import ru.shadewallcorp.itdepart.order.elastic.Order;
import ru.shadewallcorp.itdepart.order.elastic.OrderCompositionItem;
import ru.shadewallcorp.itdepart.order.elastic.SkuItem;
import ru.shadewallcorp.itdepart.order.elastic.SkuSubItem;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import javax.money.MonetaryAmount;

import static java.lang.Thread.sleep;
import static org.taymyr.lagom.elasticsearch.indices.dsl.DynamicType.FALSE;
import static org.taymyr.lagom.elasticsearch.indices.dsl.DynamicType.TRUE;
import static ru.shadewallcorp.itdepart.order.elastic.ElasticRepository.ORDER_INDEX;
import static ru.shadewallcorp.itdepart.order.elastic.ElasticRepository.ORDER_TYPE;

/**
 * Методы работы с ES, подготовка эластика для автотестов.
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
final class ElasticSearchTestService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchTest.class);

    // Сколько заказов сгенерировать
    public static final int GEN_ORDER_COUNT = 100;
    public static final String TEST_ORDER_CITY_RUS = "Москва";
    public static final String TEST_ORDER_CLIENT_PREF = "client # ";
    public static final String TEST_ORDER_COMMENT = "Комментарий кириллическими буквами для заказа";
    public static final String TEST_ORDER_COMMENT_FMT = TEST_ORDER_COMMENT + " #%d";
    public static final String TEST_ORDER_ITEM_STATE = "Без статуса";
    public static final String TEST_ORDER_ITEM_STATE_PREFIX = "Статус_";
    public static final String TEST_ORDER_FRONT = "тестовое название продукта из данного каталога";
    public static final String TEST_ORDER_FRONT_PREFIX = "продукт с названием FRONT_";
    public static final String TEST_ORDER_REGION_RUS = "Москва";
    public static final String TEST_ORDER_SALEPOINT_RUS = "Точка 1";
    public static final String TEST_ORDER_FRONT_SUBSTR_TERM_1 = "данный";
    public static final String TEST_ORDER_FRONT_SUBSTR_TERM_2 = "продукт";
    public static final String TEST_ORDER_SKU_TYPE_SEARCHABLE_PREFIX = "Тестовый";
    public static final String TEST_ORDER_SKU_TYPE = TEST_ORDER_SKU_TYPE_SEARCHABLE_PREFIX + " СКУ";

    public static final String TEST_ORDER_CATALOGUE_FILEDS_PREFIX = "- item";
    public static final String TEST_ORDER_CITY_PREF = "City ";
    public static final String TEST_ORDER_COMM_ID = "Новый comm_id";
    public static final String TEST_ORDER_REGION_PREF = "region_";
    public static final String TEST_ORDER_PRODUCT_ID_PREFIX = "PRODUCT";
    public static final String TEST_ORDER_PRODUCT_ID = TEST_ORDER_PRODUCT_ID_PREFIX + " DEFAULT_000000";
    public static final String TEST_ORDER_SKU_ID = "SKU00000000";
    public static final String TEST_ORDER_SKU_SUB_ITEM_TYPE = "sku_sub_type";

    public static final Integer EMBEDDED_ELASTIC_HTTP_PORT = 9350;
    public static final String EMBEDDED_ELASTIC_HTTP = "http://localhost:" + EMBEDDED_ELASTIC_HTTP_PORT;
    // Индекс для вложенных полей состава заказа
    public static final String ORDER_COMPOSITION_FIELD = "orderComposition";
    public static final String ORDER_COMPOSITION_AGGREGATED_FIELD = "orderCompositionAggregated";

    private EmbeddedElastic embeddedElastic;
    @Getter
    private ElasticDocument elasticDocument;
    @Getter
    private ElasticSearch elasticSearch;
    @Getter
    private ElasticIndices elasticIndices;
    private LagomClientFactory clientFactoryES;
    private boolean isStarted;

    /**
     * Создать интерфейсы служб ES.
     */
    ElasticSearchTestService() throws URISyntaxException {
        clientFactoryES = LagomClientFactory.create("elastic-search", LagomClientFactory.class.getClassLoader());
        elasticDocument = clientFactoryES.createClient(ElasticDocument.class, new URI(EMBEDDED_ELASTIC_HTTP));
        elasticSearch = clientFactoryES.createClient(ElasticSearch.class, new URI(EMBEDDED_ELASTIC_HTTP));
        elasticIndices = clientFactoryES.createClient(ElasticIndices.class, new URI(EMBEDDED_ELASTIC_HTTP));
    }

    void finalizeElasticSearch() {
        if (clientFactoryES != null) {
            clientFactoryES.close();
        }
        if (embeddedElastic != null) {
            embeddedElastic.stop();
        }
    }

    void startElasticSearchServer()
            throws IOException, InterruptedException  {
        if (!isStarted) {
            embeddedElastic = EmbeddedElastic.builder()
                    .withElasticVersion("6.4.1")
                    .withSetting(PopularProperties.HTTP_PORT, EMBEDDED_ELASTIC_HTTP_PORT)
                    // This setting prevents indices to be turned to the 'read-only' state
                    .withSetting("cluster.routing.allocation.disk.threshold_enabled", false)
                    .withStartTimeout(5, TimeUnit.MINUTES)
                    .withCleanInstallationDirectoryOnStop(false)
                    .build()
                    .start();
            isStarted = true;
        }
    }

    /**
     * Заполнение тестовыми данными.
     */
    void fillElasticSearch() {
        // создать индексы по полям фильтра для поиска заказов
        createIndex(ORDER_INDEX, ElasticSearchTestService::getElasticOrderIndexMapProperties, false);
        // заполнить репозиторий
        orderEventConsumerTestGenerateData();
    }

    /**
     * Генерировать свойства для структурного поля по его полям.
     *
     * @param fields - список полей вложенного объекта (любое может быть вложенным объектом)
     * @return свойство поля в ES
     */
    private static MappingProperty simpleObjectFieldType(Map<String, MappingProperty> fields) {
        return MappingProperty.builder()
                .type(DataType.OBJECT)
                .properties(fields).build();
    }

    /**
     * Генерировать свойства для структурного поля по его полям.
     *
     * @param fields - список полей вложенного объекта (любое может быть вложенным объектом)
     * @return свойство поля в ES
     */
    private static MappingProperty nestedObjectFieldType(Map<String, MappingProperty> fields) {
        return MappingProperty.builder()
                .type(DataType.NESTED)
                .properties(fields).build();
    }

    /**
     * Создать свойство для текстового поля c анализатором семантики русского языка (для поддержки полнотекстового поиска).
     * @return свойство поля для добавления в индекс.
     */
    private static MappingProperty mappingPropertyTextRus() {
        return MappingProperty.builder().type(DataType.TEXT).analyzer("russian").build();
    }

    /**
     * Создать свойство для текстового поля строки состава заказа с учетом агрегирования данных в специальное поле заказа.
     * @param dataType тип поля данных.
     * @return свойство поля для добавления в индекс.
     */
    private static MappingProperty mappingPropertyAggr(@NotNull DataType dataType) {
        return MappingProperty.builder().type(dataType).copyTo(ORDER_COMPOSITION_AGGREGATED_FIELD).build();
    }

    /**
     * Получить карту полей элементов состава заказа для регистрации индекса в elastic.
     */
    private static Map<String, MappingProperty> getOrderCompositionIndexMapProperties() {
        // состав заказа - составное поле, необходимо собрать меппинги для него
        Map<String, MappingProperty> priceFields = new HashMap<>();
        priceFields.put("amount", MappingProperty.DOUBLE);
        priceFields.put("currency", mappingPropertyAggr(DataType.KEYWORD));
        Map<String, MappingProperty> salePriceFields = new HashMap<>(priceFields);
        Map<String, MappingProperty> catalogueIdFields = new HashMap<>();
        catalogueIdFields.put("productId", mappingPropertyAggr(DataType.KEYWORD));
        catalogueIdFields.put("skuId", mappingPropertyAggr(DataType.KEYWORD));
        catalogueIdFields.put("serialId", mappingPropertyAggr(DataType.KEYWORD));
        Map<String, MappingProperty> skuSubItemFields = new HashMap<>();
        skuSubItemFields.put("articleNumber", mappingPropertyAggr(DataType.KEYWORD));
        skuSubItemFields.put("catalogueId", simpleObjectFieldType(catalogueIdFields));
        skuSubItemFields.put("type", mappingPropertyAggr(DataType.KEYWORD));
        skuSubItemFields.put("caption", mappingPropertyAggr(DataType.TEXT));
        skuSubItemFields.put("attrFlag", mappingPropertyAggr(DataType.KEYWORD));
        skuSubItemFields.put("amount", MappingProperty.INTEGER);
        skuSubItemFields.put("price", simpleObjectFieldType(priceFields));
        Map<String, MappingProperty> skuItemFields = new HashMap<>(skuSubItemFields);
        skuItemFields.put("salePrice", simpleObjectFieldType(salePriceFields));
        // В ES нет типа "массив", каждое поле может иметь одно и более значений одного типа
        Map<String, MappingProperty> orderCompositionFields = new HashMap<>();
        orderCompositionFields.put("item", simpleObjectFieldType(skuItemFields));
        orderCompositionFields.put("subItem", simpleObjectFieldType(skuSubItemFields));
        orderCompositionFields.put("itemState", mappingPropertyAggr(DataType.KEYWORD));
        return orderCompositionFields;
    }

    /**
     * Получить карту полей фильтра для регистрации индекса в elastic.
     */
    private static Map<String, MappingProperty> getElasticOrderIndexMapProperties() {
        Map<String, MappingProperty> res = new HashMap<>();
        res.put("id", MappingProperty.KEYWORD);
        res.put("creationDate", MappingProperty.DATE);
        res.put("orderState", MappingProperty.KEYWORD);
        res.put("orderType", MappingProperty.KEYWORD);
        res.put("region", MappingProperty.KEYWORD);
        res.put("city", MappingProperty.KEYWORD);
        res.put("salePointId", MappingProperty.KEYWORD);
        res.put("salePointAddress", mappingPropertyTextRus());
        res.put("shipGroupType", MappingProperty.INTEGER);
        res.put("clientName", mappingPropertyTextRus());
        res.put("clientEmail", mappingPropertyTextRus());
        res.put("clientPhone", mappingPropertyTextRus());
        res.put("totalPrice", MappingProperty.DOUBLE);
        res.put("payType", MappingProperty.KEYWORD);
        res.put("payState", MappingProperty.KEYWORD);
        res.put("shpi", mappingPropertyTextRus());
        res.put("comment", mappingPropertyTextRus());
        res.put("attrFlag", mappingPropertyTextRus());
        res.put("addressDelivery", mappingPropertyTextRus());
        res.put("promo", MappingProperty.KEYWORD);
        res.put("parentNetwork", mappingPropertyTextRus());
        res.put("network", mappingPropertyTextRus());
        res.put("partner", mappingPropertyTextRus());
        res.put("cancelReason", MappingProperty.KEYWORD);
        res.put(ORDER_COMPOSITION_AGGREGATED_FIELD, mappingPropertyTextRus());
        res.put(ORDER_COMPOSITION_FIELD, simpleObjectFieldType(getOrderCompositionIndexMapProperties()));
        // Страницы поиска - отдельные параметры поиска, не для индекса
        return res;
    }

    /**
     * Создать индекс по полям фильтра для поиска заказов.
     */
    private void createIndex(String indexName, Supplier<Map<String, MappingProperty>> getIndexMapProperties,
                                    boolean canModifyIndex) {
        Map<String, Mapping> elasticIndexMap = new HashMap<>();
        elasticIndexMap.put(indexName, new Mapping(getIndexMapProperties.get(), canModifyIndex ? TRUE : FALSE));
        CreateIndex elasticIndex = new CreateIndex(
                new CreateIndex.Settings(5, 1, null),
                elasticIndexMap);
        try {
            LOGGER.info(String.format("[BeforeALL] Index json=[%s]", (new ObjectMapper()).writeValueAsString(elasticIndex)));
        } catch (JsonProcessingException e) {
            LOGGER.info(String.format("[BeforeALL] Index creating: error while forming json: %s", e.getMessage()));
        }
        elasticIndices.create(indexName)
                .invoke(elasticIndex)
                .exceptionally(error -> {
                            LOGGER.info(String.format("[BeforeALL] Index not created: err=%s", error.getMessage()));
                            return null;
                        }
                )
                .thenApply(createIndexResult -> {
                    LOGGER.info(String.format("[BeforeALL] Index created: %s", createIndexResult.component3()));
                    embeddedElastic.refreshIndices();
                    return createIndexResult;
                })
                .toCompletableFuture();
    }

    /**
     * Вернуть непустое значение или значение по умолчанию.
     */
    private static <T> T nvl(T value, T defValue) {
        return value != null ? value : defValue;
    }

    /**
     * Вернуть непустое значение или рандомно: случайное значение или фиксированное значение по умолчанию.
     */
    private static String nvlRandom(String value, String defValuePref, String defValueStatic, Random random) {
        // Магические числа для генерации разномастных данных
        final int randomSomeObjId = 1000;
        return value != null ? value : random.nextInt(10) < 7 ? defValuePref + random.nextInt(randomSomeObjId) : defValueStatic;
    }

    /**
     * Вернуть непустое значение или рандомно: случайное значение или null.
     */
    private static String nvlRandomAttrFlag(String value, Random random) {
        // Магические числа для генерации разрядов номера телефона
        return value != null ? value : random.nextInt(10) < 3 ? null :
                String.format("8-977-%03d-%02d-%02d", random.nextInt(1000), random.nextInt(100), random.nextInt(100));
    }

    /**
     * Генерировать вложенное поле для заказа.
     */
    private static MonetaryAmount generateOrderFieldCurrencyAmount(Random random, int maxRandomVal) {
        return Money.of(random.nextInt(maxRandomVal) / 100.0, "RUB");
    }

    /**
     * Генерировать вложенное поле для заказа.
     */
    private static CatalogueId generateOrderFieldCatalogueId(Random random, @NotNull String valuesPrefix) {
        return CatalogueId.builder()
                .productId(nvlRandom(null, TEST_ORDER_PRODUCT_ID_PREFIX, TEST_ORDER_PRODUCT_ID, random) + valuesPrefix)
                .skuId(nvlRandom(null, "", TEST_ORDER_SKU_ID, random) + valuesPrefix)
                .serialId(nvlRandom(null, "", null, random) + valuesPrefix)
                .build();
    }

    private static SkuSubItem generateOrderFieldSkuSubItem(Random random, int maxRandomVal) {
        // catalogueId есть не у всех записей - для проверки поиска по каталогу
        CatalogueId catalogueId = null;
        if (random.nextInt(10) < 7) {
            catalogueId = generateOrderFieldCatalogueId(random, "");
        }
        return SkuSubItem.builder()
                .articleNumber(nvlRandom(null, "SUB_COMM_ID_", null, random))
                .type(nvlRandom(null, "SUB_TYPE_", TEST_ORDER_SKU_SUB_ITEM_TYPE, random))
                .caption(nvlRandom(null, "SUB_FRONT_", null, random))
                .catalogueId(catalogueId)
                .amount(random.nextInt(maxRandomVal))
                .build();
    }

    private static SkuItem generateOrderFieldSkuItem(Random random, int maxRandomVal) {
        // catalogueId есть не у всех записей - для проверки поиска по каталогу
        CatalogueId catalogueId = null;
        if (random.nextInt(10) < 7) {
            catalogueId = generateOrderFieldCatalogueId(random, TEST_ORDER_CATALOGUE_FILEDS_PREFIX);
        }
        return SkuItem.builder()
                .articleNumber(nvlRandom(null, "COMM_ID_", TEST_ORDER_COMM_ID, random))
                .type(nvlRandom(null, "TYPE_", TEST_ORDER_SKU_TYPE, random))
                .caption(nvlRandom(null, TEST_ORDER_FRONT_PREFIX, TEST_ORDER_FRONT, random))
                .attrFlag(nvlRandom(null, "", null, random))
                .catalogueId(catalogueId)
                .amount(random.nextInt(maxRandomVal))
                .price(generateOrderFieldCurrencyAmount(random, maxRandomVal))
                .salePrice(generateOrderFieldCurrencyAmount(random, maxRandomVal / 10))
                .build();
    }

    @NotNull
    private static List<OrderCompositionItem> getOrderFieldsComposition(Random random, int maxRandomVal) {
        List<OrderCompositionItem> res = new ArrayList<>();
        // У 20% заказов будет состав, у 4% состав из двух элементов.
        while (res.isEmpty() || random.nextInt(10) < 2) {
            res.add(OrderCompositionItem.builder()
                    .item(generateOrderFieldSkuItem(random, maxRandomVal))
                    .subItem(generateOrderFieldSkuSubItem(random, maxRandomVal))
                    .itemState(nvlRandom(null, TEST_ORDER_ITEM_STATE_PREFIX, TEST_ORDER_ITEM_STATE, random))
                    .build());
            if (res.size() > 2) {
                LOGGER.info(String.format("Count of items in composition: %d", res.size()));
            }
        }
        return res;
    }

    /**
     * Генерация заказа со случайными данными на основе шаблона, в котором можно указать необходимые поля (напр., ID).
     *
     * @param source шаблон с необходимыми заполненными полями
     * @return запись со случайными данными, некоторые поля могут не быть заполненными.
     */
    private static Order fillRandomOrder(Order source) {
        Random random = new Random(DateTime.now().getMillis() * Long.parseLong(source.getId()));
        // Магические числа для генерации разномастных данных
        final int maxRandomVal = 1000;
        final int randomDayFromShift = 200;
        PSequence<OrderCompositionItem> orderComposition = source.getOrderComposition();
        if (orderComposition == null || orderComposition.isEmpty()) {
            orderComposition = TreePVector.from(getOrderFieldsComposition(random, maxRandomVal));
        }
        return Order.builder()
                .id(source.getId())
                .creationDate(ZonedDateTime.now().minusDays(random.nextInt(randomDayFromShift)).minusMinutes(random.nextInt(maxRandomVal)))
                .orderState(nvlRandom(source.getOrderState(), "state ", "state new", random))
                .orderType(nvlRandom(source.getOrderType(), "type ", "type new", random))
                .region(nvlRandom(source.getRegion(), TEST_ORDER_REGION_PREF, TEST_ORDER_REGION_RUS, random))
                .city(nvlRandom(source.getCity(), TEST_ORDER_CITY_PREF, TEST_ORDER_CITY_RUS, random))
                .salePointId(nvlRandom(source.getSalePointId(), "#", TEST_ORDER_SALEPOINT_RUS, random))
                .salePointAddress(nvlRandom(source.getSalePointAddress(), "address ", null, random))
                .shipGroupType(nvl(source.getShipGroupType(), random.nextInt(maxRandomVal)))
                .clientName(nvlRandom(source.getClientName(), TEST_ORDER_CLIENT_PREF, null, random))
                .totalPrice(new BigDecimal(random.nextInt(maxRandomVal) / 100.0, new MathContext(2, RoundingMode.HALF_UP)))
                .payType(nvlRandom(source.getPayType(), "type ", "pay type new", random))
                .payState(nvlRandom(source.getPayState(), "pay state ", "pay state new", random))
                .shpi(nvlRandom(source.getShpi(), "RM", null, random))
                .comment(source.getComment() + random.nextLong())
                .attrFlag(nvlRandomAttrFlag(source.getAttrFlag(), random))
                .addressDelivery(nvlRandom(source.getAddressDelivery(), "", null, random))
                .clientEmail(nvlRandom(source.getClientEmail(), "client", null, random) + "@company.com")
                .clientPhone(nvlRandom(source.getClientPhone(), "", null, random))
                .orderComposition(orderComposition)
                .promo(nvlRandom(source.getPromo(), "", null, random))
                .parentNetwork(nvlRandom(source.getParentNetwork(), "", null, random))
                .network(nvlRandom(source.getNetwork(), "", null, random))
                .partner(nvlRandom(source.getPartner(), "", null, random))
                .cancelReason(nvlRandom(source.getCancelReason(), "", null, random))
                .build();
    }

    /**
     * Сгенерировать тестовые данные - заказы.
     */
    private void orderEventConsumerTestGenerateData() {
        final int pauseMSAfterSend = 100;
        // Сгенерировать несколько (maxId) заказов с последовательными идентификаторами
        for (Integer i = 1; i <= GEN_ORDER_COUNT; i++) {
            insertOrderDirect(fillRandomOrder(
                    Order.builder().id(i.toString()).comment(String.format(TEST_ORDER_COMMENT_FMT, i)).build()));
            LOGGER.info(String.format("[TEST#orderEventConsumer] ... sent order id=%s", i));
            try {
                // Для перестройки индекса эластику нужно время.
                sleep(pauseMSAfterSend);
            } catch (InterruptedException e) {
                LOGGER.info(String.format("[TEST#orderEventConsumer] ... error while sending order id=%s", i));
            }
        }
    }

    private void insertOrderDirect(Order order) {
        DocUpdateRequest request =
                DocUpdateRequest.builder().doc(order).docAsUpsert(true).build();
        ServiceCall.invoke(elasticDocument.update(ORDER_INDEX, ORDER_TYPE, order.getId()), request)
                .thenAccept(updateResult -> LOGGER.info(String.format("OrderSimple inserted to ES: %s", updateResult)));
    }

}
