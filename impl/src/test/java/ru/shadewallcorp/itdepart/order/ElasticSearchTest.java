package ru.shadewallcorp.itdepart.order;

import akka.NotUsed;
import akka.util.ByteString;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.lightbend.lagom.javadsl.api.deser.MessageSerializer;
import com.lightbend.lagom.javadsl.api.deser.StrictMessageSerializer;
import com.lightbend.lagom.javadsl.testkit.ServiceTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.taymyr.lagom.elasticsearch.ServiceCall;
import org.taymyr.lagom.elasticsearch.deser.ElasticSerializerFactory;
import org.taymyr.lagom.elasticsearch.document.ElasticDocument;
import org.taymyr.lagom.elasticsearch.indices.ElasticIndices;
import org.taymyr.lagom.elasticsearch.search.ElasticSearch;
import org.taymyr.lagom.elasticsearch.search.dsl.HitResult;
import org.taymyr.lagom.elasticsearch.search.dsl.Hits;
import org.taymyr.lagom.elasticsearch.search.dsl.SearchRequest;
import ru.shadewallcorp.itdepart.order.api.FilterOrder;
import ru.shadewallcorp.itdepart.order.api.OrderResponse;
import ru.shadewallcorp.itdepart.order.api.OrderSimpleResponse;
import ru.shadewallcorp.itdepart.order.elastic.ElasticRepository;
import ru.shadewallcorp.itdepart.order.elastic.OrderCardSearchResult;
import ru.shadewallcorp.itdepart.order.elastic.OrderSearchResult;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Singleton;

import static com.lightbend.lagom.javadsl.testkit.ServiceTest.bind;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static java.util.Optional.ofNullable;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.GEN_ORDER_COUNT;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_CITY_RUS;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_CLIENT_PREF;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_COMMENT;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_FRONT;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_FRONT_SUBSTR_TERM_1;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_FRONT_SUBSTR_TERM_2;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_ITEM_STATE_PREFIX;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_REGION_RUS;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_SALEPOINT_RUS;
import static ru.shadewallcorp.itdepart.order.ElasticSearchTestService.TEST_ORDER_SKU_TYPE_SEARCHABLE_PREFIX;
import static ru.shadewallcorp.itdepart.order.elastic.Converters.ESToApi;

/**
 * Тестирование сервиса заказов.
 * Тесты запускать отдельно (кнопка слева от кода class ElasticSearchTest).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@DisplayName("Test OrderSimple Service - ES")
@Disabled
final class ElasticSearchTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchTest.class);

    private static final String S_ASSERT_ORDER_COMP_NOT_FOUND_BY_PREFIX =
            "Не найден заказ по части значения статуса строки состава заказа.";
    private static final String S_ASSERT_ORDER_COMP_NOT_FOUND_BY_WORD_FORM =
            "Неправильно найдены заказы по одному склонению слова в составе заказа.";
    private static final String S_ASSERT_ORDER_COMP_NOT_FOUND_BY_WORD_FORMS =
            "Неправильно найдены заказы по склонениям слова в составе заказа.";
    private static final String S_ASSERT_ORDER_COMP_NOT_FOUND_PHRASE_PREFIX = "Не найден заказ по части значения поля состава заказа item.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_ID_FMT = "Не найден %s по ID.";
    private static final String S_ASSERT_RECORD_FOUND_FAKE_BY_ID_FMT = "Ошибочно найден %s по несуществующему ID.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_REGION_FMT = "Не найден %s по региону.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_CITY_FMT = "Не найден %s по городу.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_SALEPOINT_FMT = "Не найден %s по точке продаж.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_COMMENT_PART_FMT = "Не найден %s по части комментария.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_CLIENT_PART_FMT = "Не найден %s по части имени клиента.";
    private static final String S_ASSERT_RECORD_NOT_FOUND_BY_DATE_RANGE_FMT = "Не найден %s по диапазону дат.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_FALSE =
            "Найдено слишком мало записей \"%s\" без поля attrFlag.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_TRUE = "Найдено слишком мало записей \"%s\" с полем attrFlag.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_VOID =
            "Найдено слишком мало записей \"%s\" без учета поля attrFlag.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_PROMO_FALSE = "Найдено слишком мало записей \"%s\" без поля promo.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_PROMO_TRUE = "Найдено слишком мало записей \"%s\" c полем promo.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_PROMO_VOID = "Найдено слишком мало записей \"%s\" без учета поля promo.";
    private static final String S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_PROMO_TRUE =
            "Найдено слишком мало записей \"%s\" c полями attrFlag и promo.";
    private static final String S_TYPE_ORDER = "заказ";
    private static final String S_ACCERT_CANT_DESERIALIZE_ES_SEARCH_RESPONSE = "Не удалось десериализовать ответ ES (класс %s).";

    private static ServiceTest.TestServer testServer;
    private static ElasticSearchTestService es;

    private ElasticSearchTest() {
        //
    }

    @BeforeAll
    static void beforeAll() throws Exception {
        // подготовить службы ES
        es = new ElasticSearchTestService();
        ServiceTest.Setup setup = defaultSetup().configureBuilder(
            builder -> builder.overrides(
                    // интерфейсы внешнего или встроенного ES для прямой записи
                    bind(ElasticDocument.class).toInstance(es.getElasticDocument()),
                    bind(ElasticSearch.class).toInstance(es.getElasticSearch()),
                    bind(ElasticIndices.class).toInstance(es.getElasticIndices()),
                    // клиент с логированием
                    bind(ElasticRepository.class).to(ElasticRepositoryStub.class)
            )
        );
        testServer = ServiceTest.startServer(setup);
        // Тесты запускать по отдельности, если падает при запуске testServer
        es.startElasticSearchServer();
        // подготовить эластик: создать индексы, заполнить данными
        es.fillElasticSearch();
    }

    @AfterAll
    static void afterAll() {
        es.finalizeElasticSearch();
        if (testServer != null) {
            testServer.stop();
        }
    }

    /**
     * Проверка десериализации ответов.
     * Ошибка может быть при десериализации самих данных (OrderSimple) или при десериализации служебных данных (dsl эластика).
     */
    @Nested
    @DisplayName("elastic answer deserialization test")
    class DeserializeESResponseTest {
        private <R> R deserializeHits(Class<R> clazz, ByteString response) {
            StrictMessageSerializer<R> messageSerializer =
                    (new ElasticSerializerFactory()).messageSerializerFor(clazz);
            MessageSerializer.NegotiatedDeserializer<R, ByteString> deserializer =
                    messageSerializer.deserializer(messageSerializer.acceptResponseProtocols().get(0));
            return deserializer.deserialize(response);
        }

        @Test
        @DisplayName("must deserialize ES answer with Hits<List<HitResult<OrderSimple>>>")
        void testDeserializeHits() {
            final String order = "{\"id\": \"14\"," +
                    "\"creationDate\": \"2019-01-17T13:00:08.507+03:00\"," +
                    "\"orderState\": \"state 665\"," +
                    "\"orderType\": \"type new\"," +
                    "\"region\": \"Москва\"," +
                    "\"city\": \"City 744\"," +
                    "\"salePointId\": \"#761\"," +
                    "\"salePointAddress\": \"address 343\"," +
                    "\"shipGroupType\": 469," +
                    "\"totalPrice\": 6.4," +
                    "\"payType\": \"type 271\"," +
                    "\"payState\": \"pay state new\"," +
                    "\"comment\": \"Комментарий кириллическими буквами для заказа #148727568862787057712\"," +
                    "\"addressDelivery\": \"264\"," +
                    "\"clientEmail\": \"client@company.z849\"," +
                    "\"clientPhone\": \"892\"," +
                    "\"orderComposition\": []," +
                    "\"promo\": \"407\"," +
                    "\"parentNetwork\": \"431\"," +
                    "\"network\": \"795\"," +
                    "\"cancelReason\": \"501\"}";
            final String hitResult = "{\"_index\": \"order\"," +
                    "\"_type\": \"order\"," +
                    "\"_id\": \"1\"," +
                    "\"_score\": 1," +
                    "\"_source\":" +
                    order +
                    "}";
            final String hits = "{" +
                    "   \"total\": 1," +
                    "   \"max_score\": 1," +
                    "   \"hits\":[" +
                    hitResult +
                    "]}";
            final String response = "{\"took\": 7," +
                    "\"timed_out\": false," +
                    "\"hits\":" +
                    hits +
                    "}";
            assertAll("Десериализация ответа ES на запрос поиска заказов.",
                () -> assertThat(deserializeHits(OrderSearchResult.class, ByteString.fromString(response)))
                        .withFailMessage(
                                String.format(S_ACCERT_CANT_DESERIALIZE_ES_SEARCH_RESPONSE, OrderSearchResult.class.getSimpleName()))
                        .isNotNull()
            );
        }
    }

    /** Test class.
     */
    @Nested
    @DisplayName("Find orders by its fields")
    class FindOrdersByFieldsTest {

        private List<OrderSimpleResponse> invokeFindOrdersViaElastic(FilterOrder filterOrder) {
            ElasticRepository repository = testServer.injector().instanceOf(ElasticRepository.class);
            return repository.findOrders(filterOrder).toCompletableFuture().join();
        }

        private List<OrderResponse> invokeGetOrderViaElastic(Long id) {
            ElasticRepository repository = testServer.injector().instanceOf(ElasticRepository.class);
            return repository.getOrder(id).toCompletableFuture().join();
        }

        /**
         * Поиск заказа через elastic напрямую.
         * Поиск по ID, городу, региону, комментарию, клиенту, дате.
         */
        @Test
        @DisplayName("orders must be found by separate fields directly in ES")
        void testFindOrdersByFieldsViaElastic() {
            findOrdersByFields(this::invokeFindOrdersViaElastic);
        }

        /**
         * Поиск заказов указанным методом.
         */
        private void findOrdersByFields(Function<FilterOrder, List<OrderSimpleResponse>> search) {
            final int dateSearchRangeWidth = 300;
            assertAll("Поиск заказа по ID, городу, региону, комментарию, клиенту, дате.",
                () -> assertThat(search.apply(FilterOrder.builder().id(1L).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_ID_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        .anyMatch(p -> p.getOrderSimple().getId().startsWith("1")),
                () -> assertThat(search.apply(FilterOrder.builder().id(-1L).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_FOUND_FAKE_BY_ID_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isEmpty(),
                () -> assertThat(search.apply(FilterOrder.builder().region(TEST_ORDER_REGION_RUS).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_REGION_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        .anyMatch(p -> p.getOrderSimple().getRegion().equals(TEST_ORDER_REGION_RUS)),
                () -> assertThat(search.apply(FilterOrder.builder().city(TEST_ORDER_CITY_RUS).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_CITY_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        .anyMatch(p -> p.getOrderSimple().getCity().equals(TEST_ORDER_CITY_RUS)),
                () -> assertThat(search.apply(FilterOrder.builder().salePointId(TEST_ORDER_SALEPOINT_RUS).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_SALEPOINT_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        .anyMatch(p -> p.getOrderSimple().getSalePointId().equals(TEST_ORDER_SALEPOINT_RUS)),
                () -> assertThat(search.apply(FilterOrder.builder().comment(TEST_ORDER_COMMENT).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_COMMENT_PART_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        // У лида и заказа разные комментарии
                        .anyMatch(p -> p.getOrderSimple().getComment().startsWith(TEST_ORDER_COMMENT)),
                () -> assertThat(search.apply(FilterOrder.builder().clientName(TEST_ORDER_CLIENT_PREF).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_CLIENT_PART_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        .anyMatch(p -> p.getOrderSimple().getClientName().startsWith(TEST_ORDER_CLIENT_PREF)),
                () -> assertThat(search.apply(FilterOrder.builder()
                        .dateFrom(ZonedDateTime.now().minusDays(dateSearchRangeWidth)).dateTo(ZonedDateTime.now()).build()))
                        .withFailMessage(String.format(S_ASSERT_RECORD_NOT_FOUND_BY_DATE_RANGE_FMT, S_TYPE_ORDER))
                        .isNotNull()
                        .isNotEmpty()
                        .anyMatch(p -> ZonedDateTime.now().isAfter(p.getOrderSimple().getCreationDate()))
            );
        }

        /**
         * Поиск заказов с учетом сложных фильтров указанным способом.
         */
        private void findOrdersByFlags(Function<FilterOrder, List<OrderSimpleResponse>> search,
                                       Function<Long, List<OrderResponse>> get) {
            final int pageSize = 100;
            assertAll("Поиск заказа по признакам заполненности и незаполненности полей attrFlagи promo.",
                // Проверки по наличию/отсутствию promo и attrFlag
                () -> assertThat(search.apply(FilterOrder.builder().checkAttrFlag(Boolean.TRUE).build())
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o -> o.getOrder().getOrderSimple().getAttrFlag() == null)))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_TRUE, S_TYPE_ORDER))
                        .isEmpty(),
                () -> assertThat(search.apply(FilterOrder.builder().checkAttrFlag(Boolean.FALSE).build())
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o -> o.getOrder().getOrderSimple().getAttrFlag() != null)))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_FALSE, S_TYPE_ORDER))
                        .isEmpty(),
                () -> assertThat(search.apply(FilterOrder.builder().checkAttrFlag(null).pageSize(pageSize).build()))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_VOID, S_TYPE_ORDER))
                        .isNotNull()
                        .hasSize(pageSize),
                () -> assertThat(search.apply(FilterOrder.builder().checkPromo(Boolean.TRUE).build())
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o -> o.getOrder().getPromo() == null)))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_PROMO_TRUE, S_TYPE_ORDER))
                        .isEmpty(),
                () -> assertThat(search.apply(FilterOrder.builder().checkPromo(Boolean.FALSE).build())
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o -> o.getOrder().getPromo() != null)))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_PROMO_FALSE, S_TYPE_ORDER))
                        .isEmpty(),
                () -> assertThat(search.apply(FilterOrder.builder().checkPromo(null).pageSize(pageSize).build()))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_PROMO_VOID, S_TYPE_ORDER))
                        .isNotNull()
                        .hasSize(pageSize),
                () -> assertThat(search.apply(FilterOrder.builder().checkAttrFlag(Boolean.TRUE).checkPromo(Boolean.TRUE).build())
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o -> (o.getOrder().getOrderSimple().getAttrFlag() == null) || (o.getOrder().getPromo() == null))))
                        .withFailMessage(String.format(S_ASSERT_TOO_LESS_RECORD_FOUND_BY_ATTR_FLAG_PROMO_TRUE, S_TYPE_ORDER))
                        .isEmpty()
            );
        }

        /**
         * Поиск заказов с учетом сложных фильтров напрямую через ES.
         */
        @Test
        @DisplayName("orders must be found by flags \"attrFlag\" and \"promo\", by composition string directly in ES")
        void findOrdersByFlagsViaElastic() {
            findOrdersByFlags(this::invokeFindOrdersViaElastic, this::invokeGetOrderViaElastic);
        }

        /**
         * Поиск заказов по их составу.
         */
        private void findOrdersByComposition(Function<FilterOrder, List<OrderSimpleResponse>> search,
                                             Function<Long, List<OrderResponse>> get) {
            String testComposition1 = String.format("aaa %s bbb", TEST_ORDER_FRONT_SUBSTR_TERM_1);
            String testComposition2 = String.format("aaa %s bbb", TEST_ORDER_FRONT_SUBSTR_TERM_2);
            String testComposition3 =
                    TEST_ORDER_SKU_TYPE_SEARCHABLE_PREFIX.substring(0, TEST_ORDER_SKU_TYPE_SEARCHABLE_PREFIX.length() / 2);
            // по данной подстроке сформировать запрос и получить список заказов
            Function<String, List<OrderSimpleResponse>> getOrders =
                composition -> search.apply(FilterOrder.builder().orderComposition(composition).pageSize(GEN_ORDER_COUNT).build());
            assertAll("Поиск заказа по его составу.",
                () -> assertThat(getOrders.apply(TEST_ORDER_ITEM_STATE_PREFIX))
                        .withFailMessage(S_ASSERT_ORDER_COMP_NOT_FOUND_BY_PREFIX)
                        .isNotNull()
                        .isNotEmpty(),
                // для каждого найденного заказа вернуть 1 если нет состава или в составе хоть раз встречается TEST_ORDER_FRONT,
                // просуммировать - должно получиться количество всех заказов
                () -> assertThat(getOrders.apply(testComposition1)
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o ->  o.getOrder().getOrderComposition().stream().noneMatch(
                                    line -> line.getItem().getCaption().equals(TEST_ORDER_FRONT) ||
                                            line.getSubItem().getCaption().equals(TEST_ORDER_FRONT))
                                )))
                        .withFailMessage(S_ASSERT_ORDER_COMP_NOT_FOUND_BY_WORD_FORM)
                        .isEmpty(),
                // поиск по термину, заданному TEST_ORDER_FRONT_SUBSTR_TERM_2, термин должен быть во всех записях заказа, имеющих состав
                () -> assertThat(getOrders.apply(testComposition2)
                        .stream()
                        .flatMap(order -> new ArrayList<>(get.apply(Long.parseLong(order.getOrderSimple().getId())))
                                .stream()
                                .filter(o -> o.getOrder().getOrderComposition().stream().noneMatch(
                                    line -> line.getItem().getCaption().contains(TEST_ORDER_FRONT_SUBSTR_TERM_2)
                                ))
                                .filter(o -> o.getOrder().getOrderComposition().stream().noneMatch(
                                    line -> line.getSubItem().getCaption().contains(TEST_ORDER_FRONT_SUBSTR_TERM_2)
                                ))
                        ))
                        .withFailMessage(S_ASSERT_ORDER_COMP_NOT_FOUND_BY_WORD_FORMS)
                        .isNotNull()
                        .isEmpty(),
                // поиск по части строки из состава заказа
                () -> assertThat(search.apply(FilterOrder.builder().orderComposition(testComposition3).build()))
                        .withFailMessage(S_ASSERT_ORDER_COMP_NOT_FOUND_PHRASE_PREFIX)
                        .isNotNull()
                        .isNotEmpty()
            );
        }

        /**
         * Поиск заказов с учетом сложных фильтров.
         */
        @Test
        @DisplayName("orders must be found by composition string directly in ES")
        void findOrdersByCompositionViaElastic() {
            findOrdersByComposition(this::invokeFindOrdersViaElastic, this::invokeGetOrderViaElastic);
        }

    }

    /**
     * Репозиторий с логированием, для тестирования.
     *
     * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
     */
    @Singleton
    static class ElasticRepositoryStub extends ElasticRepository {

        @Inject
        ElasticRepositoryStub(ElasticDocument elasticDocument, ElasticSearch elasticSearch) {
            super(elasticDocument, elasticSearch);
        }

        @Override
        public CompletionStage<List<OrderSimpleResponse>> findOrders(FilterOrder filterOrder) {
            try {
                LOGGER.info(String.format("request json: [FILTER]%s[/FILTER]",
                        ElasticSerializerFactory.getMAPPER().writeValueAsString(filterOrder)));
            } catch (JsonProcessingException e) {
                LOGGER.info(String.format("filter json: <error> %s", e.getMessage()));
            }
            SearchRequest searchRequest = getFindOrdersRequest(filterOrder);
            try {
                LOGGER.info(String.format("request json: [REQUEST]%s[/REQUEST]",
                        ElasticSerializerFactory.getMAPPER().writeValueAsString(searchRequest)));
            } catch (JsonProcessingException e) {
                LOGGER.info(String.format("request json: <error> %s", e.getMessage()));
            }
            return findOrdersByRequest(searchRequest);
        }

        @Override
        public CompletionStage<List<OrderResponse>> getOrder(Long id) {
            return ServiceCall.invoke(
                    es.getElasticDocument().getSource(ORDER_INDEX, ORDER_TYPE, id.toString()),
                    NotUsed.getInstance(), OrderCardSearchResult.class)
                    .thenApply(result -> ofNullable(result)
                            .map(OrderCardSearchResult::getHits)
                            .map(Hits::getHits)
                            .map(Collection::stream).orElse(Stream.empty())
                            .map(HitResult::getSource)
                            .map(ESToApi::toOrderResponse)
                            .peek(order -> LOGGER.info(String.format("Get(%d)=%s", id, order)))
                            .collect(Collectors.toList())
                    );
        }

    }

}
