package ru.shadewallcorp.itdepart.order;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;
import org.taymyr.lagom.elasticsearch.document.ElasticDocument;
import org.taymyr.lagom.elasticsearch.search.ElasticSearch;
import ru.shadewallcorp.itdepart.order.api.OrderService;
import ru.shadewallcorp.itdepart.order.application.OrderServiceImpl;
import ru.shadewallcorp.itdepart.order.elastic.ElasticRepository;

/**
 * OrderSimple module.
 * https://github.com/lagom/lagom-recipes/blob/master/mixed-persistence/mixed-persistence-java-sbt/hello-impl/src/main/java/com/lightbend/
 *   lagom/recipes/mixedpersistence/hello/impl/HelloModule.java
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
public class Module extends AbstractModule implements ServiceGuiceSupport {

    @Override
    protected void configure() {
        bindService(OrderService.class, OrderServiceImpl.class);
        bind(ElasticRepository.class).asEagerSingleton();
        bindClient(ElasticDocument.class);
        bindClient(ElasticSearch.class);
    }
}
