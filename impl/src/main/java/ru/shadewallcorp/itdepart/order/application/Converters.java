package ru.shadewallcorp.itdepart.order.application;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;
import ru.shadewallcorp.itdepart.order.api.CatalogueId;
import ru.shadewallcorp.itdepart.order.api.Order;
import ru.shadewallcorp.itdepart.order.api.OrderCompositionItem;
import ru.shadewallcorp.itdepart.order.api.SkuItem;
import ru.shadewallcorp.itdepart.order.api.SkuSubItem;

import java.util.Collection;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * Конвертеры данных для персистентной сущности.
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
class Converters {

    /**
     * Конвертер классов из классы данных персистентной сущности в Kafka.
     */
    static class ApiToES {
        /**
         * Конвертировать заказ из заказа персистентной сущности в заказ ES.
         *
         * @param source заказ персистентной сущности
         * @return заказ ES.
         */
        @NotNull
        static ru.shadewallcorp.itdepart.order.elastic.Order toOrder(@NotNull Order source) {
            return ru.shadewallcorp.itdepart.order.elastic.Order.builder()
                    .id(source.getOrderSimple().getId())
                    .creationDate(source.getOrderSimple().getCreationDate())
                    .orderState(source.getOrderSimple().getOrderState())
                    .orderType(source.getOrderSimple().getOrderType())
                    .region(source.getOrderSimple().getRegion())
                    .city(source.getOrderSimple().getCity())
                    .salePointId(source.getOrderSimple().getSalePointId())
                    .salePointAddress(source.getOrderSimple().getSalePointAddress())
                    .shipGroupType(source.getOrderSimple().getShipGroupType())
                    .clientName(source.getOrderSimple().getClientName())
                    .totalPrice(source.getOrderSimple().getTotalPrice())
                    .payType(source.getOrderSimple().getPayType())
                    .payState(source.getOrderSimple().getPayState())
                    .shpi(source.getOrderSimple().getShpi())
                    .comment(source.getOrderSimple().getComment())
                    .attrFlag(source.getOrderSimple().getAttrFlag())
                    .addressDelivery(source.getAddressDelivery())
                    .clientEmail(source.getClientEmail())
                    .clientPhone(source.getClientPhone())
                    .orderComposition(toOrderComposition(source.getOrderComposition()))
                    .promo(source.getPromo())
                    .parentNetwork(source.getParentNetwork())
                    .network(source.getNetwork())
                    .partner(source.getPartner())
                    .cancelReason(source.getCancelReason())
                    .build();
        }

        /**
         * Копировать список объектов.
         * @param source исходный список (@Nullable)
         * @return копия
         */
        @NotNull
        private static PSequence<ru.shadewallcorp.itdepart.order.elastic.OrderCompositionItem> toOrderComposition(
                @Nullable PSequence<OrderCompositionItem> source) {
            return ofNullable(source)
                    .map(Collection::stream)
                    .map(stream -> stream.map(ApiToES::toOrderCompositionItem).collect(toList()))
                    .map(TreePVector::from)
                    .orElse(TreePVector.empty());
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.elastic.OrderCompositionItem toOrderCompositionItem(OrderCompositionItem source) {
            return ru.shadewallcorp.itdepart.order.elastic.OrderCompositionItem.builder()
                    .item(source.getItem() != null ? toSkuItem(source.getItem()) : null)
                    .subItem(source.getSubItem() != null ? toSkuSubItem(source.getSubItem()) : null)
                    .itemState(source.getItemState())
                    .build();
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.elastic.SkuItem toSkuItem(SkuItem source) {
            return ru.shadewallcorp.itdepart.order.elastic.SkuItem.builder()
                    .articleNumber(source.getArticleNumber())
                    .catalogueId(source.getCatalogueId() != null ? toCatalogueId(source.getCatalogueId()) : null)
                    .type(source.getType())
                    .caption(source.getCaption())
                    .attrFlag(source.getAttrFlag())
                    .amount(source.getAmount())
                    .price(source.getPrice())
                    .salePrice(source.getSalePrice())
                    .build();
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.elastic.SkuSubItem toSkuSubItem(SkuSubItem source) {
            return ru.shadewallcorp.itdepart.order.elastic.SkuSubItem.builder()
                    .articleNumber(source.getArticleNumber())
                    .catalogueId(source.getCatalogueId() != null ? toCatalogueId(source.getCatalogueId()) : null)
                    .type(source.getType())
                    .caption(source.getCaption())
                    .amount(source.getAmount())
                    .price(source.getPrice())
                    .salePrice(source.getSalePrice())
                    .build();
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.elastic.CatalogueId toCatalogueId(CatalogueId source) {
            return ru.shadewallcorp.itdepart.order.elastic.CatalogueId.builder()
                    .productId(source.getProductId())
                    .skuId(source.getSkuId())
                    .serialId(source.getSerialId())
                    .build();
        }
    }

}
