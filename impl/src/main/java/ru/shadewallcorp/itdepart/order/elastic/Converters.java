package ru.shadewallcorp.itdepart.order.elastic;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;
import ru.shadewallcorp.itdepart.order.api.Order;
import ru.shadewallcorp.itdepart.order.api.OrderResponse;
import ru.shadewallcorp.itdepart.order.api.OrderSimple;
import ru.shadewallcorp.itdepart.order.api.OrderSimpleResponse;

import java.util.Collection;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * Конвертеры типов из/в ElasticSearch.
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
public class Converters {

    /**
     * Конвертер классов ElasticSearch в API-классы.
     */
    public static class ESToApi {
        /**
         * Конвертировать заказ из ES в формат доменной модели сервиса заказов.
         *
         * @param source     заказ из ES.
         * @param totalPages количество страниц в результате.
         * @return заказ сервиса заказов.
         */
        @NotNull
        static OrderSimpleResponse toOrderSimpleResponse(@NotNull ru.shadewallcorp.itdepart.order.elastic.Order source, long totalPages) {
            return OrderSimpleResponse.builder()
                    .orderSimple(OrderSimple.builder()
                            .id(source.getId())
                            .creationDate(source.getCreationDate())
                            .orderState(source.getOrderState())
                            .orderType(source.getOrderType())
                            .region(source.getRegion())
                            .city(source.getCity())
                            .salePointId(source.getSalePointId())
                            .salePointAddress(source.getSalePointAddress())
                            .shipGroupType(source.getShipGroupType())
                            .clientName(source.getClientName())
                            .totalPrice(source.getTotalPrice())
                            .payType(source.getPayType())
                            .payState(source.getPayState())
                            .shpi(source.getShpi())
                            .comment(source.getComment())
                            .attrFlag(source.getAttrFlag())
                            .build()
                    )
                    .totalPages(totalPages)
                    .build();
        }

        /**
         * Конвертировать заказ из ES в формат доменной модели сервиса заказов.
         *
         * @param source заказ из ES.
         * @return заказ сервиса заказов.
         */
        @NotNull
        public static OrderResponse toOrderResponse(@NotNull ru.shadewallcorp.itdepart.order.elastic.Order source) {
            return OrderResponse.builder()
                    .order(Order.builder()
                            .orderSimple(OrderSimple.builder()
                                    .id(source.getId())
                                    .creationDate(source.getCreationDate())
                                    .orderState(source.getOrderState())
                                    .orderType(source.getOrderType())
                                    .region(source.getRegion())
                                    .city(source.getCity())
                                    .salePointId(source.getSalePointId())
                                    .salePointAddress(source.getSalePointAddress())
                                    .shipGroupType(source.getShipGroupType())
                                    .clientName(source.getClientName())
                                    .totalPrice(source.getTotalPrice())
                                    .payType(source.getPayType())
                                    .payState(source.getPayState())
                                    .shpi(source.getShpi())
                                    .comment(source.getComment())
                                    .attrFlag(source.getAttrFlag())
                                    .build())
                            .addressDelivery(source.getAddressDelivery())
                            .clientEmail(source.getClientEmail())
                            .clientPhone(source.getClientPhone())
                            .orderComposition(toOrderComposition(source.getOrderComposition()))
                            .promo(source.getPromo())
                            .parentNetwork(source.getParentNetwork())
                            .network(source.getNetwork())
                            .partner(source.getPartner())
                            .cancelReason(source.getCancelReason())
                            .build())
                    .build();
        }

        /**
         * Копировать список объектов.
         *
         * @param source исходный список (@Nullable)
         * @return копия
         */
        @NotNull
        private static PSequence<ru.shadewallcorp.itdepart.order.api.OrderCompositionItem> toOrderComposition(
                @Nullable PSequence<OrderCompositionItem> source) {
            return ofNullable(source)
                    .map(Collection::stream)
                    .map(stream -> stream.map(ESToApi::toOrderCompositionItem).collect(toList()))
                    .map(TreePVector::from)
                    .orElse(TreePVector.empty());
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         *
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.api.OrderCompositionItem toOrderCompositionItem(OrderCompositionItem source) {
            return ru.shadewallcorp.itdepart.order.api.OrderCompositionItem.builder()
                    .item(source.getItem() != null ? toSkuItem(source.getItem()) : null)
                    .subItem(source.getSubItem() != null ? toSkuSubItem(source.getSubItem()) : null)
                    .itemState(source.getItemState())
                    .build();
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         *
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.api.SkuItem toSkuItem(SkuItem source) {
            return ru.shadewallcorp.itdepart.order.api.SkuItem.builder()
                    .articleNumber(source.getArticleNumber())
                    .catalogueId(source.getCatalogueId() != null ? toCatalogueId(source.getCatalogueId()) : null)
                    .type(source.getType())
                    .caption(source.getCaption())
                    .attrFlag(source.getAttrFlag())
                    .amount(source.getAmount())
                    .price(source.getPrice())
                    .salePrice(source.getSalePrice())
                    .build();
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         *
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.api.SkuSubItem toSkuSubItem(SkuSubItem source) {
            return ru.shadewallcorp.itdepart.order.api.SkuSubItem.builder()
                    .articleNumber(source.getArticleNumber())
                    .catalogueId(source.getCatalogueId() != null ? toCatalogueId(source.getCatalogueId()) : null)
                    .type(source.getType())
                    .caption(source.getCaption())
                    .amount(source.getAmount())
                    .price(source.getPrice())
                    .salePrice(source.getSalePrice())
                    .build();
        }

        /**
         * Конвертировать объект в такой же, но другого слоя.
         *
         * @return сконвертированный объект
         */
        @NotNull
        private static ru.shadewallcorp.itdepart.order.api.CatalogueId toCatalogueId(CatalogueId source) {
            return ru.shadewallcorp.itdepart.order.api.CatalogueId.builder()
                    .productId(source.getProductId())
                    .skuId(source.getSkuId())
                    .serialId(source.getSerialId())
                    .build();
        }
    }

}
