package ru.shadewallcorp.itdepart.order.elastic;

import lombok.Getter;
import org.taymyr.lagom.elasticsearch.search.dsl.SearchResult;

/**
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Getter
public class OrderCardSearchResult extends SearchResult<Order> {
}
