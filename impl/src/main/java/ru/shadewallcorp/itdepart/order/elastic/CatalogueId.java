package ru.shadewallcorp.itdepart.order.elastic;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

/**
 * Характеристика catalogueId продукта (ElasticSearch).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Value
@Builder
public class CatalogueId {
    private final String productId;
    private final String skuId;
    private final String serialId;

    @JsonCreator
    public CatalogueId(@JsonProperty("productId") String productId,
                     @JsonProperty("skuId") String skuId,
                     @JsonProperty("serialId") String serialId) {
        this.productId = productId;
        this.skuId = skuId;
        this.serialId = serialId;
    }

}
