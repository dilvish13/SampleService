package ru.shadewallcorp.itdepart.order.elastic;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import javax.money.MonetaryAmount;

/**
 * Характеристика продукта (ElasticSearch).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Value
@Builder
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
public class SkuItem {
    private final String articleNumber;
    private final CatalogueId catalogueId;
    private final String type;
    private final String caption;
    private final String attrFlag;
    private final Integer amount;
    private final MonetaryAmount price;
    private final MonetaryAmount salePrice;

}
