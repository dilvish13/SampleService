package ru.shadewallcorp.itdepart.order.elastic;

import akka.NotUsed;
import org.taymyr.lagom.elasticsearch.ServiceCall;
import org.taymyr.lagom.elasticsearch.document.ElasticDocument;
import org.taymyr.lagom.elasticsearch.document.dsl.update.DocUpdateRequest;
import org.taymyr.lagom.elasticsearch.document.dsl.update.UpdateResult;
import org.taymyr.lagom.elasticsearch.search.ElasticSearch;
import org.taymyr.lagom.elasticsearch.search.dsl.Hits;
import org.taymyr.lagom.elasticsearch.search.dsl.SearchRequest;
import org.taymyr.lagom.elasticsearch.search.dsl.query.Query;
import org.taymyr.lagom.elasticsearch.search.dsl.query.compound.BoolQuery;
import ru.shadewallcorp.itdepart.order.api.FilterOrder;
import ru.shadewallcorp.itdepart.order.api.OrderResponse;
import ru.shadewallcorp.itdepart.order.api.OrderSimpleResponse;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.inject.Singleton;

import static java.math.BigDecimal.ROUND_UP;
import static java.util.Optional.ofNullable;
import static ru.shadewallcorp.itdepart.order.api.FilterOrder.DEFALUT_ES_PAGE_SIZE;
import static ru.shadewallcorp.itdepart.order.elastic.Converters.ESToApi;

/**
 * elastic elastic.
 * Used to retrieve information about orders.
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Singleton
public class ElasticRepository {

    public static final String ORDER_INDEX = "order";
    public static final String ORDER_TYPE = "order";
    private final ElasticDocument elasticDocument;
    private final ElasticSearch elasticSearch;

    @Inject
    public ElasticRepository(ElasticDocument elasticDocument, ElasticSearch elasticSearch) {
        this.elasticDocument = elasticDocument;
        this.elasticSearch = elasticSearch;
    }

    /**
     * Поиск закаов по индексу.
     *
     * @param filterOrder параметры поиска
     * @return {@code CompletionStage<List<OrderSimple>>}
     */
    public CompletionStage<List<OrderSimpleResponse>> findOrders(FilterOrder filterOrder) {
        return findOrdersByRequest(getFindOrdersRequest(filterOrder));
    }

    /**
     * Вычислить количество страниц в результате на основе количества записей.
     * @param recordCount количество записей, которое возвращает эластик
     * @param pageSize размер страницы, передаваемый в запросе
     * @return количество страниц в результате
     */
    private static int calcTotalPageCount(Integer recordCount, Integer pageSize) {
        return BigDecimal.valueOf(recordCount, 0)
                .divide(BigDecimal.valueOf(pageSize != null ? pageSize : DEFALUT_ES_PAGE_SIZE, 0), ROUND_UP).intValue();
    }

    protected CompletionStage<List<OrderSimpleResponse>> findOrdersByRequest(SearchRequest searchRequest) {
        Function<Hits<Order>, Stream<OrderSimpleResponse>> getOrderResponseList = hits ->
                hits.getHits()
                .stream()
                .map(hit -> ESToApi.toOrderSimpleResponse(hit.getSource(), calcTotalPageCount(hits.getTotal(), searchRequest.getSize())));
        return ServiceCall.invoke(elasticSearch.search(ORDER_INDEX), searchRequest, OrderSearchResult.class)
                .thenApply(result -> ofNullable(result)
                        .map(OrderSearchResult::getHits)
                        .map(getOrderResponseList)
                        .orElse(Stream.empty())
                        .collect(Collectors.toList()));
    }

    /**
     * Поиск закаов по индексу.
     *
     * @param filterOrder параметры поиска
     * @return {@code CompletionStage<List<Order>>}
     */
    public CompletionStage<List<OrderResponse>> findOrderCards(FilterOrder filterOrder) {
        return findOrderCardsByRequest(getFindOrdersRequest(filterOrder));
    }

    private CompletionStage<List<OrderResponse>> findOrderCardsByRequest(SearchRequest searchRequest) {
        return ServiceCall.invoke(elasticSearch.search(ORDER_INDEX), searchRequest, OrderSearchResult.class)
                .thenApply(result -> ofNullable(result)
                        .map(OrderSearchResult::getHits)
                        .map(hits -> hits.getHits()
                                .stream()
                                .map(hit -> ESToApi.toOrderResponse(hit.getSource()))
                        )
                        .orElse(Stream.empty())
                        .collect(Collectors.toList()));
    }

    protected SearchRequest getFindOrdersRequest(FilterOrder filterOrder) {
        // Добавить поля для поиска по полному совпадению терма/ключевого слова
        List<Query> filter = OrderSearchUtil.getTermFromFilter(filterOrder);
        // Добавить поля для поиска по частичному совпадению терма/ключевого слова
        filter.addAll(OrderSearchUtil.getPrefixFromFilter(filterOrder));
        // Добавить поля для поиска по частичному совпадению текста
        filter.addAll(OrderSearchUtil.getMatchFromFilter(filterOrder));
        // Добавить поля для поиска по диапазонам
        filter.addAll(OrderSearchUtil.getRangeFromFilter(filterOrder));
        // Добавить поиск по составу заказа
        filter.addAll(OrderSearchUtil.getMultiMatchFromFilter(filterOrder));
        BoolQuery.Builder queryBuilder = BoolQuery.builder();
        queryBuilder.filter(filter);
        // Добавить признаки заполненности / незаполненности полей
        List<Query> existFlags = OrderSearchUtil.getExistFlagsFromFilter(filterOrder);
        queryBuilder.must(existFlags);
        List<Query> nonExistFlags = OrderSearchUtil.getNonExistFlagsFromFilter(filterOrder);
        queryBuilder.mustNot(nonExistFlags);
        return new SearchRequest(queryBuilder.build(), filterOrder.getFrom(), filterOrder.getPageSize());
    }

    /**
     * Получить заказ (полный) по ID.
     * @param id ID
     * @return {@code CompletionStage<Done>}
     */
    public CompletionStage<List<OrderResponse>> getOrder(Long id) {
        return ServiceCall.invoke(
                elasticDocument.getSource(ORDER_INDEX, ORDER_TYPE, id.toString()), NotUsed.getInstance(), OrderCardSearchResult.class)
                .thenApply(result -> ofNullable(result)
                        .map(orderCardSrcResult -> orderCardSrcResult.getTyped("", ru.shadewallcorp.itdepart.order.elastic.Order.class))
                        .map(Collections::singletonList)
                        .map(Collection::stream).orElse(Stream.empty())
                        .map(ESToApi::toOrderResponse)
                        .collect(Collectors.toList())
                );
    }

    /**
     * Сохранить данные заказа в ES.
     *
     * @param orderES заказ в формате эластика
     * @return {@code CompletionStage<Done>}
     */
    public CompletionStage<String> storeOrder(ru.shadewallcorp.itdepart.order.elastic.Order orderES) {
        DocUpdateRequest request =
                DocUpdateRequest.builder().doc(orderES).docAsUpsert(true).build();
        return ServiceCall.invoke(elasticDocument.update(ORDER_INDEX, ORDER_TYPE, orderES.getId()), request)
                .thenApply(UpdateResult::getId);
    }

}
