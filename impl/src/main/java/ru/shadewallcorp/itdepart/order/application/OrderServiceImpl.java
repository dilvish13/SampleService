package ru.shadewallcorp.itdepart.order.application;

import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.typesafe.config.Config;
import org.taymyr.lagom.javadsl.openapi.AbstractOpenAPIService;
import ru.shadewallcorp.itdepart.order.api.FilterOrder;
import ru.shadewallcorp.itdepart.order.api.Order;
import ru.shadewallcorp.itdepart.order.api.OrderResponse;
import ru.shadewallcorp.itdepart.order.api.OrderService;
import ru.shadewallcorp.itdepart.order.api.OrderSimpleResponse;
import ru.shadewallcorp.itdepart.order.elastic.ElasticRepository;

import java.util.List;
import javax.inject.Inject;

import static ru.shadewallcorp.itdepart.order.application.Converters.ApiToES;

/**
 * OrderSimple service implementation.
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
public class OrderServiceImpl extends AbstractOpenAPIService implements OrderService {

    private ElasticRepository elasticRepository;

    @Inject
    public OrderServiceImpl(Config config, ElasticRepository elasticRepository) {
        super(config);
        this.elasticRepository = elasticRepository;
    }

    /**
     * Получить заказ (полный) по ID.
     * @return {@link ServiceCall}
     */
    @Override
    public ServiceCall<NotUsed, List<OrderResponse>> getOrder(Long id) {
        return NotUsed -> elasticRepository.findOrderCards(FilterOrder.builder().id(id).build()).handle(this::handleError);
    }

    /**
     * Получить список всех заказов.
     * @return {@link ServiceCall}
     */
    @Override
    public ServiceCall<FilterOrder, List<OrderSimpleResponse>> findOrders() {
        return filterOrder -> elasticRepository.findOrders(filterOrder).handle(this::handleError);
    }

    /**
     * Записать заказ.
     * @return {@link ServiceCall}
     */
    @Override
    public ServiceCall<Order, String> insertOrder() {
        return order -> elasticRepository.storeOrder(ApiToES.toOrder(order));
    }

    private <V> V handleError(V value, Throwable throwable) {
        if (throwable != null) {
            throw new OrderServiceSystemException(throwable.getMessage());
        } else {
            return value;
        }
    }

}
