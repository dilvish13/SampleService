package ru.shadewallcorp.itdepart.order.elastic;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.pcollections.PSequence;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * Заказ (ElasticSearch).
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
@Value
@AllArgsConstructor(onConstructor = @__({@JsonCreator}))
@Builder
public class Order {

    private final String id;
    @JsonFormat(pattern = "yyyy.MM.dd'T'HH:mm:ssZ")
    private final ZonedDateTime creationDate;
    private final String orderState;
    private final String orderType;
    private final String region;
    private final String city;
    private final String salePointId;
    private final String salePointAddress;
    private final Integer shipGroupType;
    private final String clientName;
    private final BigDecimal totalPrice;
    private final String payType;
    private final String payState;
    private final String shpi;
    private final String comment;
    private final String addressDelivery;
    private final String clientEmail;
    private final String clientPhone;
    private final PSequence<OrderCompositionItem> orderComposition;
    private final String promo;
    private final String parentNetwork;
    private final String network;
    private final String partner;
    private final String cancelReason;
    /** Набор атрибутов. */
    private final String attrFlag;

}
