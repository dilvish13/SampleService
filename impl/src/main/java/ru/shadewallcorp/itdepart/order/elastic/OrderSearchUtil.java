package ru.shadewallcorp.itdepart.order.elastic;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.taymyr.lagom.elasticsearch.search.dsl.query.Query;
import org.taymyr.lagom.elasticsearch.search.dsl.query.fulltext.MatchPhrasePrefixQuery;
import org.taymyr.lagom.elasticsearch.search.dsl.query.fulltext.MatchQuery;
import org.taymyr.lagom.elasticsearch.search.dsl.query.term.ExistsQuery;
import org.taymyr.lagom.elasticsearch.search.dsl.query.term.PrefixQuery;
import org.taymyr.lagom.elasticsearch.search.dsl.query.term.TermQuery;
import ru.shadewallcorp.itdepart.order.api.FilterOrder;
import ru.shadewallcorp.itdepart.order.elastic.SearchTermsUtil.DateTermRangeUtils;

import java.util.ArrayList;
import java.util.List;

import static ru.shadewallcorp.itdepart.order.elastic.SearchTermsUtil.addQueryIfValueNotBlank;
import static ru.shadewallcorp.itdepart.order.elastic.SearchTermsUtil.addQueryIfValueNotBlankRange;

/**
 * Утилитарный класс со списком полей для поиска.
 *
 * @author M. Golovatiy {@literal <grant.all.on.any.table@gmail.com>}
 */
final class OrderSearchUtil {

    private OrderSearchUtil() {
        //
    }

    /**
     * Получить список параметров для поиска.
     * Поля для поиска по полному совпадению терма/ключевого слова.
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getTermFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> terms = new ArrayList<>();
        addQueryIfValueNotBlank(terms, TermQuery::of, "OrderState", filterOrder.getOrderState());
        addQueryIfValueNotBlank(terms, TermQuery::of, "OrderType", filterOrder.getOrderType());
        addQueryIfValueNotBlank(terms, TermQuery::of, "SalePointId", filterOrder.getSalePointId());
        addQueryIfValueNotBlank(terms, TermQuery::of, "Region", filterOrder.getRegion());
        addQueryIfValueNotBlank(terms, TermQuery::of, "City", filterOrder.getCity());
        addQueryIfValueNotBlank(terms, TermQuery::of, "ShipGroupType", filterOrder.getShipGroupType());
        addQueryIfValueNotBlank(terms, TermQuery::of, "PayType", filterOrder.getPayType());
        addQueryIfValueNotBlank(terms, TermQuery::of, "PayState", filterOrder.getPayState());
        addQueryIfValueNotBlank(terms, TermQuery::of, "Shpi", filterOrder.getShpi());
        addQueryIfValueNotBlank(terms, TermQuery::of, "Promo", filterOrder.getPromo());
        // Параметры страниц передаются отдельно
        return terms;
    }

    /**
     * Получить список параметров для поиска.
     * Поля для поиска по частичному совпадению терма/ключевого слова.
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getPrefixFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> termPrefixes = new ArrayList<>();
        addQueryIfValueNotBlank(termPrefixes, PrefixQuery::of, "Id", filterOrder.getId());
        // Параметры страниц передаются отдельно
        return termPrefixes;
    }

    /**
     * Получить список параметров для поиска.
     * Поля для поиска по частичному совпадению текста.
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getMatchFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> matches = new ArrayList<>();
        addQueryIfValueNotBlank(matches, MatchQuery::of, "ClientName", filterOrder.getClientName());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "ClientEmail", filterOrder.getClientEmail());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "ClientPhone", filterOrder.getClientPhone());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "Comment", filterOrder.getComment());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "ParentNetwork", filterOrder.getParentNetwork());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "Network", filterOrder.getNetwork());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "Partner", filterOrder.getPartner());
        addQueryIfValueNotBlank(matches, MatchQuery::of, "CancelReason", filterOrder.getCancelReason());
        // Параметры страниц передаются отдельно
        return matches;
    }

    /**
     * Получить список параметров для поиска.
     * Поля для поиска по диапазону.
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getRangeFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> ranges = new ArrayList<>();
        addQueryIfValueNotBlankRange(ranges, filterOrder.getDateFrom(), filterOrder.getDateTo(),
                DateTermRangeUtils::getRange, DateTermRangeUtils::ofRange);
        // Параметры страниц передаются отдельно
        return ranges;
    }

    /**
     * Получить список параметров для поиска.
     * Поиск по признаку заполненности полей.
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getExistFlagsFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> exists = new ArrayList<>();
        if (filterOrder.checkAttrFlagNonEmpty()) {
            exists.add(ExistsQuery.of("attrFlag"));
        }
        if (filterOrder.checkPromoNonEmpty()) {
            exists.add(ExistsQuery.of("promo"));
        }
        return exists;
    }

    /**
     * Получить список параметров для поиска.
     * Поиск по признаку незаполненности полей.
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getNonExistFlagsFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> nonExists = new ArrayList<>();
        if (filterOrder.checkAttrFlagEmpty()) {
            nonExists.add(ExistsQuery.of("attrFlag"));
        }
        if (filterOrder.checkPromoEmpty()) {
            nonExists.add(ExistsQuery.of("promo"));
        }
        return nonExists;
    }

    /**
     * Получить список параметров для поиска.
     * Поиск по вложенным полям.
     * Если несколько терминов - поиск по терминам (с учетом семантики),
     * если одно слово - поиск по подстроке
     * @param filterOrder параметры поиска
     * @return список фильтров
     */
    @NotNull
    static List<Query> getMultiMatchFromFilter(@NotNull FilterOrder filterOrder) {
        List<Query> match = new ArrayList<>();
        String[] orderCompositionFilterTerms = StringUtils.split(filterOrder.getOrderComposition(), " ");
        boolean isMultiTermFiler = (orderCompositionFilterTerms != null) && (orderCompositionFilterTerms.length > 1);
        addQueryIfValueNotBlank(match, isMultiTermFiler ? MatchQuery::of : MatchPhrasePrefixQuery::of,
                "OrderComposition", filterOrder.getCancelReason());
        return match;
    }

}
